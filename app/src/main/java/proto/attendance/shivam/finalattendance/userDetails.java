package proto.attendance.shivam.finalattendance;

/**
 * Created by acer on 11-05-2018.
 */

public class userDetails {

    private String name, email, branch, year, role, attendance, mis, messBill, useless;

    userDetails(){

    }

    userDetails(String name, String mis, String email, String branch, String year, String role, String attendance, String messBill, String useless){
        this.name = name;
        this.email = email;
        this.mis = mis;
        this.branch = branch;
        this.year = year;
        this.role  = role;
        this.attendance = attendance;
        this.messBill = messBill;
        this.useless = useless;
    }

    public String getName(){ return name; };

    public String getEmail() {
        return email;
    }

    public String getBranch() {
        return branch;
    }

    public String getYear() {
        return year;
    }

    public String getMis() {
        return mis;
    }

    public String getRole() { return role; }

    public String getAttendance() { return attendance; }

    public String getMessBill() { return messBill; }
}

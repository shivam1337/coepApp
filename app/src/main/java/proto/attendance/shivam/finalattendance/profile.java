package proto.attendance.shivam.finalattendance;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class profile extends Fragment {

    FirebaseAuth mAuth;
    FirebaseUser user;
    DatabaseReference mRef;
    TextView uname, umis, ubranch, uyear, umail, urole;
    ImageView upic;
    View view;

    private OnFragmentInteractionListener mListener;

    public profile() {
        // Required empty public constructor
    }

    public static profile newInstance() {
        profile fragment = new profile();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.fragment_profile, container, false);
        uname = (TextView)view.findViewById(R.id.username);
        umis = (TextView)view.findViewById(R.id.mis);
        ubranch = (TextView)view.findViewById(R.id.branch);
        uyear = (TextView)view.findViewById(R.id.year);
        upic = (ImageView)view.findViewById(R.id.profilepic);
        umail = (TextView)view.findViewById(R.id.coepmail);
        urole = (TextView)view.findViewById(R.id.role);

        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();

        mRef = FirebaseDatabase.getInstance().getReference("user").child(user.getDisplayName());
        uname.setText(user.getDisplayName());
        Glide.with(this).applyDefaultRequestOptions(new RequestOptions().override(350, 350).circleCrop()).load(user.getPhotoUrl()).into(upic);

        mRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                userDetails userDetails = dataSnapshot.getValue(userDetails.class);
                umis.setText(userDetails.getMis());
                ubranch.setText(userDetails.getBranch());
                uyear.setText(userDetails.getYear());
                umail.setText(userDetails.getEmail());
                urole.setText(userDetails.getRole());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
/*
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
*/
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

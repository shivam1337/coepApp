package proto.attendance.shivam.finalattendance;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class attendance extends Fragment {

    private OnFragmentInteractionListener mListener;

    FirebaseAuth mAuth;
    FirebaseUser user;
    DatabaseReference mRef, mRef1;
    TextView uattendance, adate, atime, ayear, abranch;
    LinearLayout l1;

    static Boolean isClickable = false;
    View view;
    int total, prev;
    String pre;

    public attendance() {
        // Required empty public constructor
    }

    public static attendance newInstance() {
        attendance fragment = new attendance();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_attendance, container, false);

        uattendance = (TextView)view.findViewById(R.id.lectures);
        adate = (TextView)view.findViewById(R.id.date);
        atime = (TextView)view.findViewById(R.id.time);
        ayear = (TextView)view.findViewById(R.id.yr);
        abranch = (TextView)view.findViewById(R.id.br);
        l1 = (LinearLayout)view.findViewById(R.id.ated);

        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        mRef = FirebaseDatabase.getInstance().getReference("user").child(user.getDisplayName());
        mRef1 = FirebaseDatabase.getInstance().getReference("lectures");

        int uless = 1;
        updateUselss(uless);

        mRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                userDetails userDetails = dataSnapshot.getValue(userDetails.class);
                uattendance.setText(userDetails.getAttendance());
                prev = Integer.parseInt(userDetails.getAttendance());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mRef1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                atd attd = dataSnapshot.getValue(atd.class);
                adate.setText(attd.getDate());
                atime.setText(attd.getTime());
                ayear.setText(attd.getYear());
                abranch.setText(attd.getBranch());
                isClickable = true;
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
/*
        mRef1.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                atd attd = dataSnapshot.getValue(atd.class);
                adate.setText(attd.getDate());
                atime.setText(attd.getTime());
                ayear.setText(attd.getYear());
                abranch.setText(attd.getBranch());
                isClickable = true;
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
*/
        l1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isClickable){
                    isClickable = false;
                    l1.setBackgroundColor(Color.RED);
                    updateAttendance();
                }else {
                    Toast.makeText(getContext(), "Attendance already marked!", Toast.LENGTH_SHORT).show();
                }
            }
        });


        return view;
    }

    private void updateAttendance() {
        total = prev + 1;
        mRef.child("attendance").setValue(String.valueOf(total));
    }

    private void updateUselss(int fin) {
        String bill = String.valueOf(fin);
        mRef.child("useless").setValue(bill);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
/*
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
*/
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

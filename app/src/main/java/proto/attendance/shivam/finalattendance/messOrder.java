package proto.attendance.shivam.finalattendance;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class messOrder extends AppCompatActivity {

    String[] breakfast = {"Change (Rs. 13)", "MFT (Rs. 20)", "Bread Jam (Rs. 10)", "Half Fry (Rs. 15)", "Full Fry (Rs. 20)"};
    String[] beverage = {"Tea (Rs. 10)", "Coffee (Rs. 15)", "Milk (Rs. 10)"};
    String[] snack = {"Biscuits (Rs. 5)", "Cream Roll (Rs. 10)", "Khari (Rs. 7)", "Cake (Rs. 15)"};
    Spinner breakFastSpin, beverageSpin, snackSpin;

    DatabaseReference mRef;
    FirebaseAuth mAuth;

    Button submit;

    int prev;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mess_order);

        breakFastSpin = (Spinner) findViewById(R.id.changespin);
        beverageSpin = (Spinner) findViewById(R.id.bevspin);
        snackSpin = (Spinner) findViewById(R.id.snkspin);
        submit = (Button) findViewById(R.id.messorder);

        mAuth = FirebaseAuth.getInstance();

        FirebaseUser user  = mAuth.getCurrentUser();
        mRef = FirebaseDatabase.getInstance().getReference("user/"+ user.getDisplayName());
        int uless = 1;
        updateUselss(uless);

        mRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                userDetails userDetails = dataSnapshot.getValue(userDetails.class);
                prev = Integer.parseInt(userDetails.getMessBill());
                Toast.makeText(getApplicationContext(), String.valueOf(prev), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        //prev = getPrev();

        ArrayAdapter s1 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, breakfast);
        s1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        breakFastSpin.setAdapter(s1);

        final String[] brk = new String[1];
        breakFastSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                brk[0] = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ArrayAdapter s2 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, beverage);
        s2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        beverageSpin.setAdapter(s2);

        final String[] bev = new String[1];
        beverageSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                bev[0] = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ArrayAdapter s3 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, snack);
        s3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        snackSpin.setAdapter(s3);

        final String[] snk = new String[1];
        snackSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                snk[0] = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int bk, sn, bv, total;
                if(brk[0] != null){
                    bk = Integer.parseInt(brk[0].replaceAll("[^0-9]", ""));
                }else{
                    bk = 0;
                    brk[0] = "";
                }
                if(bev[0] != null){
                    bv = Integer.parseInt(bev[0].replaceAll("[^0-9]", ""));
                }else{
                    bv = 0;
                    bev[0] = "";
                }
                if(snk[0] != null){
                    sn = Integer.parseInt(snk[0].replaceAll("[^0-9]", ""));
                }else{
                    sn = 0;
                    snk[0] = "";
                }

                total = bk + bv + sn;
                updateBill(total, brk[0], bev[0], snk[0], prev);
            }
        });

    }

    private void updateBill(int total, String brk, String bev, String snk, int prev) {
        int fin;
        //prev = getPrev();

        fin = prev + total;
        update(fin);

        Toast.makeText(getApplicationContext(), "Contents ordered:\n " + brk + "\n" + bev + "\n" + snk, Toast.LENGTH_LONG).show();
        startActivity(new Intent(messOrder.this, home.class));
    }


    private void update(int fin) {
        String bill = String.valueOf(fin);
        mRef.child("messBill").setValue(bill);
    }

    private void updateUselss(int fin) {
        String bill = String.valueOf(fin);
        mRef.child("useless").setValue(bill);
    }
}

package proto.attendance.shivam.finalattendance;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class signup extends AppCompatActivity {

    EditText mis, mail;
    Spinner branch, year;
    Button submit;
    String[] braches = {"CE", "IT", "Mech", "Entc", "Elec", "Civil", "Instru", "Meta", "Planning"};
    String[]years = {"First Year", "Second Year", "Third Year", "Btech"};
    DatabaseReference mRef;
    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        mis = (EditText)findViewById(R.id.mis);
        branch = (Spinner)findViewById(R.id.branchSpin);
        year = (Spinner)findViewById(R.id.yearSpin);
        mail = (EditText)findViewById(R.id.mail);
        submit = (Button)findViewById(R.id.submit);

        ArrayAdapter s1 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, braches);
        s1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        branch.setAdapter(s1);

        ArrayAdapter s2 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, years);
        s1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        year.setAdapter(s2);

        final String[] br = new String[1];
        branch.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                br[0] = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        final String[] yr = new String[1];
        year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                yr[0] = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mAuth = FirebaseAuth.getInstance();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ubranch, uyear;
                ubranch = br[0];
                uyear = yr[0];
                addUser(ubranch, uyear);
                Intent intent = new Intent(signup.this, home.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void addUser(String ubranch, String uyear) {
        String smis, role, umail, messBill;
        Long umis;
        smis = mis.getText().toString();
        umail = mail.getText().toString();
        umis = Long.parseLong(smis);
        String attendance = String.valueOf(0);
        messBill = String.valueOf(0);

        if(umis / 100000000 == 1){
            role = "Student";

        }else if(umis / 100000000 == 4){
            role = "Teacher";
        }else{
            role = null;
        }

        String useless = String.valueOf(0);
        FirebaseUser user  = mAuth.getCurrentUser();
        mRef = FirebaseDatabase.getInstance().getReference("user");
        String uname = user.getDisplayName();

        final userDetails uDetails = new userDetails(uname, smis, umail, ubranch, uyear, role, attendance, messBill, useless);

        mRef.child(user.getDisplayName()).setValue(uDetails);

    }
}

package proto.attendance.shivam.finalattendance;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static android.content.ContentValues.TAG;

public class admin extends Fragment {

    View view;
    Spinner branch, year;
    Button submit, dateTime;
    TextView totl;
    String[] branches = {"CE", "IT", "Mech", "Entc", "Elec", "Civil", "Instru", "Meta", "Planning"};
    String[] years = {"FY", "SY", "TY", "Btech"};

    DatabaseReference mRef;
    FirebaseAuth mAuth;
    FirebaseUser user;
    Calendar date;

    String prev, uless = "useless";
    private OnFragmentInteractionListener mListener;

    public admin() {
        // Required empty public constructor
    }

    public static admin newInstance() {
        admin fragment = new admin();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_admin, container, false);

        branch = (Spinner)view.findViewById(R.id.branchSpin);
        year = (Spinner)view.findViewById(R.id.yearSpin);
        submit = (Button)view.findViewById(R.id.leccreate);
        dateTime = (Button)view.findViewById(R.id.datetime);
        totl = (TextView)view.findViewById(R.id.totallec);

        ArrayAdapter s1 = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, branches);
        s1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        branch.setAdapter(s1);

        ArrayAdapter s2 = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, years);
        s1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        year.setAdapter(s2);

        final String[] br = new String[1];
        branch.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                br[0] = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        final String[] yr = new String[1];
        year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                yr[0] = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        mRef = FirebaseDatabase.getInstance().getReference("lectures");

        updateUselss(uless);
        mRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                atd attd = dataSnapshot.getValue(atd.class);
                prev = attd.getTotlec();
                totl.setText(prev);
                Toast.makeText(getContext(), prev, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        dateTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDateTime();
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int day, month, year, hour, minute;
                day = date.get(Calendar.DAY_OF_MONTH);
                month = date.get(Calendar.MONTH);
                year = date.get(Calendar.YEAR);
                hour = date.get(Calendar.HOUR);
                minute = date.get(Calendar.MINUTE);

                String dateFin = String.valueOf(day) + "-" + String.valueOf(month+1) + "-" + String.valueOf(year);
                String timeFin = String.valueOf(hour) + "-" + String.valueOf(minute);
                updateLec(dateFin, timeFin, yr[0], br[0]);

                startActivity(new Intent(getContext(), home.class));
            }
        });

        return view;
    }

    private void updateLec(String dt, String time, String yr, String br) {
        int total;
        if(prev == null){
            total = 1;
        }else {
            total = 1 + Integer.parseInt(prev);
        }

        String tot = String.valueOf(total);
        atd attd = new atd(dt, time, yr, br, tot);
        mRef.setValue(attd);
    }

    private void setDateTime() {
        final Calendar currentDate = Calendar.getInstance();
        date = Calendar.getInstance();
        new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                date.set(year, monthOfYear, dayOfMonth);
                new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        date.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        date.set(Calendar.MINUTE, minute);
                        Log.v(TAG, "The choosen one " + date.getTime());
                    }
                }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show();
            }
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();
    }

    private void updateUselss(String uless) {
        mRef.child("useless").setValue(uless);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
/*
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
*/
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

package proto.attendance.shivam.finalattendance;

/**
 * Created by acer on 12-05-2018.
 */

public class atd {
    String date, time, year, branch, totlec;

    atd(){

    }

    atd(String date, String time, String year, String branch, String totlec){
        this.date = date;
        this.time = time;
        this.year = year;
        this.branch = branch;
        this.totlec = totlec;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getYear() {
        return year;
    }

    public String getBranch() {
        return branch;
    }

    public String getTotlec() {
        return totlec;
    }
}
